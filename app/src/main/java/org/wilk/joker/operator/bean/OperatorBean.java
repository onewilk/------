/**
 * OperatorJoker
 * org.wilk.joker.operator
 * OperatorBean.java
 * 责任人:  
 * 创建/修改时间: 2014-11-20-下午3:23:07
 * Copyright © 2013-2014 深圳掌通宝科技有限公司-版权所有
 */

package org.wilk.joker.operator.bean;

/**
 * OperatorBean 描述: 责任人: 创建/修改时间:2014-11-20 下午3:23:07 备注:
 * 
 * @version 1.0.0
 */
public class OperatorBean {
    private String name;

    private String code;

    /**
     * name
     * 
     * @return the name
     * @since 1.0.0
     */

    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * code
     * 
     * @return the code
     * @since 1.0.0
     */

    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 创建一个新的 OperatorBean实例.
     * 
     * @param name
     * @param code
     */
    public OperatorBean(String name, String code) {
        super();
        this.name = name;
        this.code = code;
    }

    /**
     * 创建一个新的 OperatorBean实例.
     */
    public OperatorBean() {
        super();
    }

}

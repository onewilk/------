/**
 * SaleOrange
 * com.ztb.sale.orange
 * SaleApplication.java
 * 责任人:  
 * 创建/修改时间: 2014-11-6-下午5:14:55
 * Copyright © 2013-2014 深圳掌通宝科技有限公司-版权所有
 */

package org.wilk.joker.operator;

import android.app.Application;
import android.content.Context;

import org.wilk.joker.operator.constant.PreferencesKey;
import org.wilk.joker.operator.utils.PreferencesUtils;

/**
 * SaleApplication 描述: 责任人: 创建/修改时间:2014-11-6 下午5:14:55 备注:
 * 
 * @version 1.0.0
 */
public class JokerApplication extends Application {

    /** instance */
    private static JokerApplication mInstance;

    /** 是否是首次进入应用 */
    public static boolean isFirstEnter(Context context) {
        return !PreferencesUtils.getBoolean(context, PreferencesKey.ALREADY_ENTER_APP);
    }

    /** 是否是备份过配置信息 */
    public static boolean isFirstBacked(Context context) {
        return !PreferencesUtils.getBoolean(context, PreferencesKey.ALREADY_BACKED);
    }

    /** 单例 */
    public static JokerApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }
}

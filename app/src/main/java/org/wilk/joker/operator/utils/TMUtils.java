/**
 * OperatorJoker
 * org.wilk.joker.operator.utils
 * TMUtils.java
 * 责任人:  
 * 创建/修改时间: 2014-11-20-下午2:48:44
 * Copyright © 2013-2014 深圳掌通宝科技有限公司-版权所有
 */

package org.wilk.joker.operator.utils;

import android.content.Context;
import android.telephony.TelephonyManager;

import java.io.File;

/**
 * TMUtils 描述: 责任人: 创建/修改时间:2014-11-20 下午2:48:44 备注:
 * 
 * @version 1.0.0
 */
public class TMUtils {
    private Context mContext;

    private TelephonyManager mTManager;

    public TMUtils(Context context) {
        this.mContext = context;
        this.mTManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
    }

    public TelephonyManager getManager() {
        return this.mTManager;
    }

    public boolean hasSpnConfXML() {
        File file = new File("/system/etc/spn-conf.xml");
        return file.exists();
    }

    public String getSimOperatorName() {
        return mTManager.getSimOperatorName();
    }

    public String getSimOperator() {
        return mTManager.getSimOperator();
    }

    public String getSimSerialNumber() {
        return mTManager.getSimSerialNumber();
    }
}

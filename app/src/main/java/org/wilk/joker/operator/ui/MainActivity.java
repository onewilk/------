package org.wilk.joker.operator.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Xml;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialogCompat;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import org.wilk.joker.operator.JokerApplication;
import org.wilk.joker.operator.R;
import org.wilk.joker.operator.bean.OperatorBean;
import org.wilk.joker.operator.constant.PreferencesKey;
import org.wilk.joker.operator.utils.LogUtils;
import org.wilk.joker.operator.utils.PreferencesUtils;
import org.wilk.joker.operator.utils.ShellUtils;
import org.wilk.joker.operator.utils.TMUtils;
import org.wilk.joker.operator.utils.ToastUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.util.ArrayList;

public class MainActivity extends ActionBarActivity implements OnClickListener {
    /**
     * 运营商名字编辑文本框
     */
    private EditText mOperaNameEditText;

    /**
     * 运营商代码编辑文本框 - 不可修改
     */
    private EditText mOperaCodeEditText;

    /**
     * CODE BY MR.WILK
     */
    private TextView mSignText;

    /**
     * 开始更改按钮
     */
    private PaperButton mChangeButton;

    /**
     * FAB菜单 - UI换色
     */
    private FloatingActionsMenu mFABMenu;

    /**
     * FAB菜单 - UI换色
     */
    private FloatingActionButton mFABTheme;

    /**
     * FAB菜单 - 重启手机
     */
    private FloatingActionButton mFABReboot;

    /**
     * FAB菜单 - 帮助信息
     */
    private FloatingActionButton mFABHelp;

    /**
     * 工具栏
     */
    private Toolbar mToolbar;

    /**
     * 加载提示框
     */
    private ProgressDialog mLoadingDialog;


    /**
     * 颜色选择框
     */
    private ColorPickerDialog mColorPickerDialog;

    /**
     * 重启提示框
     */
    private AlertDialog mRebootDialog;

    /**
     * 帮助信息提示框
     */
    private AlertDialog mHelpDialog;

    /**
     * 通信管理工具类
     */
    private TMUtils uTmUtils;

    /**
     * 可选颜色数组
     */
    private int mColors[] = {
            R.color.md_red_a200, R.color.md_red_a400, R.color.md_red_a700,
            R.color.md_purple_a200, R.color.md_purple_a400, R.color.md_purple_a700,
            R.color.md_deep_purple_a200, R.color.md_deep_purple_a400, R.color.md_deep_purple_a700,
            R.color.md_indigo_a200, R.color.md_indigo_a400, R.color.md_indigo_a700,
            R.color.md_blue_a200, R.color.md_blue_a400, R.color.md_blue_a700,
            R.color.md_light_blue_a200, R.color.md_light_blue_a400, R.color.md_light_blue_a700,
            R.color.md_cyan_a200, R.color.md_cyan_a400, R.color.md_cyan_a700,
            R.color.md_teal_a200, R.color.md_teal_a400, R.color.md_teal_a700,
            R.color.md_green_a200, R.color.md_green_a400, R.color.md_green_a700,
            R.color.md_light_green_a200, R.color.md_light_green_a400, R.color.md_light_green_a700,
            R.color.md_lime_a200, R.color.md_lime_a400, R.color.md_lime_a700,
            R.color.md_yellow_a200, R.color.md_yellow_a400, R.color.md_yellow_a700,
            R.color.md_amber_a200, R.color.md_amber_a400, R.color.md_amber_a700,
            R.color.md_orange_a200, R.color.md_orange_a400, R.color.md_orange_a700,
            R.color.md_deep_orange_a200, R.color.md_deep_orange_a400, R.color.md_deep_orange_a700
    };

    /**
     * 运营商名字
     */
    private String mOperaName;

    /**
     * 运营商代码
     */
    private String mOperaCode;

    /**
     * spn-conf.xml地址
     */
    private String SPN_CONF_XML_PATH = "/system/etc/spn-conf.xml";

    /**
     * 当前所有的spn-xml信息
     */
    private ArrayList<OperatorBean> mOperatorList = new ArrayList<OperatorBean>();

    /**
     * 标识是否已ROOT
     */
    private boolean isRooted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        initData();
        bindEvent();

        if (JokerApplication.isFirstEnter(this)) {
            showHelpInfo();
            PreferencesUtils.putBoolean(this, PreferencesKey.ALREADY_ENTER_APP, true);
        }
    }

    /**
     * 初始化view
     */
    private void initView() {
        mOperaNameEditText = (EditText) this.findViewById(R.id.et_operator_name);
        mOperaCodeEditText = (EditText) this.findViewById(R.id.et_operator_code);
        mSignText = (TextView) this.findViewById(R.id.sign);

        mFABMenu = (FloatingActionsMenu) this.findViewById(R.id.action_menu);
        mFABTheme = (FloatingActionButton) this.findViewById(R.id.action_theme);
        mFABReboot = (FloatingActionButton) this.findViewById(R.id.action_reboot);
        mFABHelp = (FloatingActionButton) this.findViewById(R.id.action_help);

        mChangeButton = (PaperButton) this.findViewById(R.id.btn_change);
        //防止焦点被EditText抢夺导致输入键盘自动弹出
        mChangeButton.requestFocus();

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }

        mLoadingDialog = new ProgressDialog(this);
        mLoadingDialog.setTitle(R.string.dialog_loading_title);
        mLoadingDialog.setMessage(getString(R.string.dialog_loading_msg));
    }

    /**
     * 初始化数据
     */
    private void initData() {
        uTmUtils = new TMUtils(this);

        mOperaName = uTmUtils.getSimOperatorName();
        mOperaCode = uTmUtils.getSimOperator();

        mOperaNameEditText.setText(mOperaName);
        mOperaNameEditText.setHint(mOperaName);
        mOperaCodeEditText.setHint(mOperaCode);
        mOperaCodeEditText.setText(mOperaCode);

        mOperaNameEditText.setSelection(mOperaName.length());
    }

    /**
     * 绑定事件
     */
    private void bindEvent() {
        mFABTheme.setOnClickListener(this);
        mFABReboot.setOnClickListener(this);
        mFABHelp.setOnClickListener(this);

        mChangeButton.setOnClickListener(this);
    }

    /**
     * 检查root权限
     */
    private void checkRoot() {
        mLoadingDialog.show();
        isRooted = ShellUtils.checkRootPermission();
        ToastUtils.show(this, isRooted ? R.string.toast_rooted : R.string.toast_unrooted);
        if (isRooted && JokerApplication.isFirstBacked(this)) {
            PreferencesUtils.putBoolean(this, PreferencesKey.ALREADY_BACKED, true);
        }
        mLoadingDialog.dismiss();
    }

    /**
     * 更改操作
     */
    private void doChange(OperatorBean item) {
        checkRoot();
        if (isRooted) {
            mLoadingDialog.show();
            initOperatorBeans();
        } else {
            return;
        }

        LogUtils.d("WILK", "数据初始化完毕");
        int index = -1;
        for (int i = 0; i < mOperatorList.size(); i++) {
            if (mOperatorList.get(i).getCode().equals(item.getCode()))
                index = i;
        }
        if (index >= 0) {
            // 已经存在运营商则进行修改
            mOperatorList.get(index).setName(item.getName());
        } else {
            // 新数据则直接写入
            mOperatorList.add(item);
        }
        saveToXml();
        mLoadingDialog.dismiss();
        showRebootDialog();
    }

    /**
     * 初始化运营商列表
     */
    public void initOperatorBeans() {
        mOperatorList.clear();
        if (!uTmUtils.hasSpnConfXML()) {
            ToastUtils.show(this, R.string.toast_file_not_found);
            return;
        } else {
            OperatorBean mOperator = null;
            XmlPullParser xpp = Xml.newPullParser();
            try {
                xpp.setInput(new FileInputStream(new File(SPN_CONF_XML_PATH)), "utf-8");
                int eventType = xpp.getEventType();
                while (eventType != XmlPullParser.END_DOCUMENT) {
                    switch (eventType) {
                        case XmlPullParser.START_TAG:
                            if ("spnOverride".equals(xpp.getName())) {
                                mOperator = new OperatorBean(xpp.getAttributeValue(1).toString(),
                                        xpp.getAttributeValue(0).toString());
                                mOperatorList.add(mOperator);
                            }
                            break;
                        default:
                            break;
                    }
                    eventType = xpp.next();
                }
            } catch (XmlPullParserException | IOException ioe) {
                ToastUtils.show(this, R.string.toast_xml_parser_error);
                return;
            }
        }
    }

    /**
     * 保存操作
     */
    public void saveToXml() {
        XmlSerializer serializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        try {
            serializer.setOutput(writer);
            serializer.startDocument("utf-8", null);
            serializer.text("\r\n");
            serializer.startTag(null, "spnOverrides");
            serializer.text("\r\n");
            for (OperatorBean item : mOperatorList) {
                serializer.startTag(null, "spnOverride");
                serializer.attribute(null, "numeric", item.getCode());
                serializer.attribute(null, "spn", item.getName());
                serializer.endTag(null, "spnOverride");
                serializer.text("\r\n");
            }
            serializer.endTag(null, "spnOverrides");
            serializer.endDocument();

            File file = new File(getCacheDir(), "temp.xml");
            if (!file.exists())
                file.createNewFile();
            OutputStream os = new FileOutputStream(file);
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
            bw.write(writer.toString());
            bw.flush();
            bw.close();
            writer.flush();
            writer.close();
            ShellUtils.execCommand("mount -o rw,remount /dev/block/mtdblock3 /system", true);
            ShellUtils.execCommand("cat " + file.getAbsolutePath() + " > " + SPN_CONF_XML_PATH,
                    true);
        } catch (IllegalArgumentException | IllegalStateException | IOException e) {
            ToastUtils.show(this, R.string.toast_xml_save_error);
            LogUtils.d("WILK", e.toString());
            return;
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_change) {
            // 点击更改按钮
            doChange(new OperatorBean(mOperaNameEditText.getText().toString(), mOperaCodeEditText
                    .getText().toString()));
        } else if (id == R.id.action_theme) {
            mFABMenu.toggle();
            // 主题切换
            showColorPickerDialog();
        } else if (id == R.id.action_reboot) {
            mFABMenu.toggle();
            // 重启手机
            showRebootDialog();
        } else if (id == R.id.action_help) {
            mFABMenu.toggle();
            // 帮助信息
            showHelpInfo();
        }
    }

    /**
     * 更改界面主题颜色
     */
    private void reloadUIColor(int color) {
        int primary = color;
        int darker = ColorPickerDialog.getDarkerColor(color);
        PreferencesUtils.putInt(MainActivity.this, PreferencesKey.CURRENT_UI_COLOR, primary);
        PreferencesUtils.putInt(MainActivity.this, PreferencesKey.CURRENT_UI_COLOR_DARK, darker);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.getWindow().setStatusBarColor(darker);
        }
        mToolbar.setBackgroundColor(primary);
        mOperaCodeEditText.setTextColor(primary);
        mOperaNameEditText.setTextColor(primary);
        mChangeButton.setBackgroundColor(primary);


        mFABMenu.setAddButtonColor(primary, darker);
        mSignText.setTextColor(darker);
    }

    /**
     * 显示主题切换对话框
     */
    private void showColorPickerDialog() {
        if (mColorPickerDialog == null) {
            mColorPickerDialog = new ColorPickerDialog.Builder(this)
                    .setTitle(getString(R.string.title_picker_color))
                    .setColors(mColors)
                    .setOnColorClickListener(new ColorPickerDialog.ColorClickListener() {
                        @Override
                        public void onColorClick(int color) {
                            reloadUIColor(color);
                        }
                    }).create();
        }
        mColorPickerDialog.show();
    }

    /**
     * 显示帮助信息
     */
    private void showHelpInfo() {
        if (mHelpDialog == null) {
            mHelpDialog = new MaterialDialogCompat.Builder(this)
                    .setTitle(R.string.action_help)
                    .setMessage(R.string.dialog_msg_help_msg)
                    .setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).create();
        }
        mHelpDialog.show();
    }

    /**
     * 重启手机的提示
     */
    private void showRebootDialog() {
        if (mRebootDialog == null) {
            mRebootDialog = new MaterialDialogCompat.Builder(this)
                    .setTitle(R.string.dialog_loading_title)
                    .setMessage(R.string.dialog_msg_reboot)
                    .setPositiveButton(R.string.dialog_btn_yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            ShellUtils.execCommand("reboot", true);
                        }
                    })
                    .setNegativeButton(R.string.dialog_btn_no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).create();
        }
        mRebootDialog.show();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
package org.wilk.joker.operator.constant;

/**
 * PreferencesConstant 
 * 
 * @version 1.0.0
 */
public class PreferencesKey {
    /** 标识是否首次进入应用 */
    public static final String ALREADY_ENTER_APP = "ALREADY_ENTER_APP";

    /** 标识是否已经备份过配置 */
    public static final String ALREADY_BACKED = "ALREADY_BACKED";

    /** 标识当前的界面颜色 - 主色 */
    public static final String CURRENT_UI_COLOR = "CURRENT_UI_COLOR";

    /** 标识当前的界面颜色 - 深色 */
    public static final String CURRENT_UI_COLOR_DARK = "CURRENT_UI_COLOR_DARK";
}
